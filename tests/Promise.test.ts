/// <reference path="../typings/index.d.ts" />


import {Promise, IFunc} from '../src/Promise';
//TODO chai 05.09.16 12:44 author Tsigel
declare let chai: any;

describe('Promise', () => {
    let assert = chai.assert;
    const TEST_TIME = 10;

    describe('sync', () => {

        describe('dynamic', () => {

            describe('then', () => {

                it('simple resolve', () => {

                    let ok;
                    let param;
                    new Promise((resolve: IFunc<boolean>) => {
                        resolve(true);
                    }).then((result: boolean) => {
                        ok = true;
                        param = result;
                    }, () => {
                        assert.fail(null, null, 'Попал в reject в успешном обещании!');
                    });

                    assert.ok(ok, 'then не отработал!');
                    assert.equal(param, true, 'Данные переданные в resolve не равны данным из then!');

                });

                it('simple reject', () => {

                    let ok;
                    let param;
                    new Promise((resolve: IFunc<{}>, reject: IFunc<boolean>) => {
                        reject(true);
                    }).then(() => {
                        assert.fail(null, null, 'Попал в success в отклоненном обещании!');
                    }, (result: boolean) => {
                        ok = true;
                        param = result;
                    });

                    assert.ok(ok, 'then не отработал!');
                    assert.equal(param, true, 'Данные переданные в reject не равны данным из then!');

                });

                it('catch constructor error', () => {

                    let ok;
                    let param;
                    new Promise((resolve: IFunc<{}>, reject: IFunc<boolean>) => {
                        throw new Error('test');
                    }).then(() => {
                        assert.fail(null, null, 'Попал в success в отклоненном обещании!');
                    }, (error: Error) => {
                        ok = true;
                        param = error.message;
                    });

                    assert.ok(ok, 'then не отработал!');
                    assert.equal(param, 'test', 'Данные переданные в reject не равны данным из then!');

                });

                it('catch then error', () => {

                    let ok;
                    let param;
                    new Promise((resolve: IFunc<{}>) => {
                        resolve(true);
                    }).then(() => {
                        throw new Error('test');
                    }, () => {
                        assert.fail(null, null, 'Попал в reject в успешном обещании!');
                    }).then(() => {
                        assert.fail(null, null, 'Попал в success в отклоненном обещании!');
                    }, (error: Error) => {
                        ok = true;
                        param = error.message;
                    });

                    assert.ok(ok, 'then не отработал!');
                    assert.equal(param, 'test', 'Данные переданные в reject не равны данным из then!');

                });

                describe('chain then', () => {

                    it('return new value', () => {

                        let ok;
                        let param;

                        new Promise((resolve: IFunc<boolean>) => {
                            resolve(true);
                        }).then((result: boolean) => {
                            if (result) {
                                return 5;
                            } else {
                                assert.fail(null, null, 'Данные переданные в reject не равны данным из then!');
                            }
                        }).then((result: number) => {
                            ok = true;
                            param = result;
                        });

                        assert.ok(ok, 'then не отработал!');
                        assert.equal(param, 5, 'Данные переданные в reject не равны данным из then!');

                    });

                    it('return new promise', () => {

                        let ok;
                        let param;

                        new Promise((resolve: IFunc<boolean>) => {
                            resolve(true);
                        }).then((result: boolean) => {
                            if (result) {
                                return new Promise((resolve: IFunc<boolean>) => resolve(true));
                            } else {
                                assert.fail(null, null, 'Данные переданные в reject не равны данным из then!');
                            }
                        }).then((result: boolean) => {
                            ok = true;
                            param = result;
                        });

                        assert.ok(ok, 'then не отработал!');
                        assert.equal(param, true, 'Данные переданные в reject не равны данным из then!');

                    });

                    it('after error handler', () => {

                        let ok;
                        let param;

                        new Promise((resolve: IFunc<boolean>, reject: IFunc<boolean>) => {
                            reject(true);
                        }).then(() => {
                            assert.fail(null, null, 'Попал в success в отклоненном обещании!');
                        }, (error: boolean) => {
                            return error;
                        }).then((result: boolean) => {
                            ok = true;
                            param = result;
                        });

                        assert.ok(ok, 'then не отработал!');
                        assert.equal(param, true, 'Данные переданные в reject не равны данным из then!');

                    });

                });

            });

            describe('catch', () => {

                it('simple resolve', () => {

                    let ok;
                    let param;

                    new Promise((resolve: IFunc<{}>, reject: IFunc<boolean>) => {
                        resolve(true);
                    }).catch(() => {
                        assert.fail(null, null, 'Попал в error в resolved обещании!');
                    }).then((data: boolean) => {
                        ok = true;
                        param = data;
                    });

                    assert.ok(ok, 'catch не отработал!');
                    assert.equal(param, true, 'Данные переданные в reject не равны данным из catch!');

                });

                it('simple reject', () => {

                    let ok;
                    let param;

                    new Promise((resolve: IFunc<{}>, reject: IFunc<boolean>) => {
                        reject(true);
                    }).catch((data: boolean) => {
                        ok = true;
                        param = data;
                    });

                    assert.ok(ok, 'catch не отработал!');
                    assert.equal(param, true, 'Данные переданные в reject не равны данным из catch!');

                });

                it('then after catch', () => {

                    let ok;
                    let param;

                    new Promise((resolve: IFunc<{}>, reject: IFunc<boolean>) => {
                        reject(true);
                    }).catch(() => {
                        return 5;
                    }).then((data: number) => {
                        ok = true;
                        param = data;
                    });

                    assert.ok(ok, 'catch не отработал!');
                    assert.equal(param, 5, 'Данные которые мы вернули в catch не попали в then!');

                });

            });

        });

        describe('static', () => {

            describe('resolve', () => {

                it('simple', () => {

                    let result;
                    Promise.resolve(5).then((data: number) => {
                        result = data;
                    }, () => {
                        assert.fail(null, null, 'Попал в error в отклоненном обещании!');
                    });
                    assert.equal(result, 5, 'Неправильные данные в then');

                });

                it('promise', () => {

                    let result;
                    Promise.resolve(Promise.resolve(5)).then((data: number) => {
                        result = data;
                    }, () => {
                        assert.fail(null, null, 'Попал в error в отклоненном обещании!');
                    });
                    assert.equal(result, 5, 'Неправильные данные в then');

                });

                it('promise like', () => {

                    Promise.resolve({
                        then: (resolve: IFunc<number>): any => {
                            resolve(5);
                        }
                    }).then((data: number) => {
                        assert.equal(data, 5);
                    });

                });

            });

            describe('rase', () => {

                it('resolve', () => {
                    Promise.race([Promise.resolve(2), Promise.resolve(5)]).then((data: number) => {
                        assert.equal(data, 2);
                    });
                });

                it('reject', () => {
                    Promise.race([Promise.reject(2), Promise.reject(5)]).catch((data: number) => {
                        assert.equal(data, 2);
                    });
                });

            });

            it('reject', () => {

                let result;
                Promise.reject(5).then(() => {
                    assert.fail(null, null, 'Попал в success в отклоненном обещании!');
                }, (data: number) => {
                    result = data;
                });
                assert.equal(result, 5, 'Неправильные данные в then');

            });

            describe('all', () => {

                it('empty', () => {

                    Promise.all([]).then((data) => {
                        assert.equal(JSON.stringify(data), '[]');
                    });

                });

                it('simple', () => {

                    let ok;
                    let params;
                    let promises = [Promise.resolve(5), Promise.resolve(2)];

                    Promise.all(promises).then((data: Array<number>) => {
                        ok = true;
                        params = data;
                    });

                    assert.ok(ok, 'all не отработал!');
                    assert.equal(params[0], 5);
                    assert.equal(params[1], 2);

                });

                it('with reject', () => {

                    let ok;
                    let params;
                    let promises = [Promise.reject(5), Promise.resolve(2)];

                    Promise.all(promises).then(() => {
                            assert.fail(null, null, 'Попал в success в отклоненном обещании!');
                        },
                        (data: number) => {
                            ok = true;
                            params = data;
                        });

                    assert.ok(ok, 'all не отработал!');
                    assert.equal(params, 5);

                });

            });

        });

    });

    describe('async', () => {

        describe('dynamic', () => {

            describe('then', () => {

                it('simple resolve', (done) => {

                    new Promise((resolve: IFunc<boolean>) => {
                        setTimeout(() => {
                            resolve(true);
                        }, TEST_TIME);
                    }).then((result: boolean) => {
                        assert.equal(result, true, 'Данные переданные в resolve не равны данным из then!');
                        done();
                    }, () => {
                        assert.fail(null, null, 'Попал в reject в успешном обещании!');
                    });

                });

                it('simple reject', (done) => {

                    new Promise((resolve: IFunc<{}>, reject: IFunc<boolean>) => {
                        setTimeout(() => reject(true), TEST_TIME);
                    }).then(() => {
                        assert.fail(null, null, 'Попал в success в отклоненном обещании!');
                    }, (result: boolean) => {
                        assert.equal(result, true, 'Данные переданные в resolve не равны данным из then!');
                        done();
                    });

                });

                describe('chain then', () => {

                    it('return new value', (done) => {

                        new Promise((resolve: IFunc<boolean>) => {
                            setTimeout(() => {
                                resolve(true);
                            }, TEST_TIME);
                        }).then((result: boolean) => {
                            if (result) {
                                return 5;
                            } else {
                                assert.fail(null, null, 'Данные переданные в reject не равны данным из then!');
                            }
                        }).then((result: number) => {
                            assert.equal(result, 5, 'Данные переданные в resolve не равны данным из then!');
                            done();
                        });

                    });

                    it('return new promise', (done) => {

                        let ok;

                        new Promise((resolve: IFunc<boolean>) => {
                            setTimeout(() => resolve(true), TEST_TIME);
                        }).then((result: boolean) => {
                            let promise = new Promise((resolve: IFunc<boolean>) => {
                                setTimeout(() => resolve(true), TEST_TIME);
                            });
                            ok = true;
                            if (result) {
                                return promise;
                            } else {
                                assert.fail(null, null, 'Данные переданные в reject не равны данным из then!');
                            }
                        }).then((result: boolean) => {
                            assert.ok(ok);
                            assert.equal(result, true, 'Данные переданные в resolve не равны данным из then!');
                            done();
                        });

                    });

                    it('after error handler', (done) => {

                        new Promise((resolve: IFunc<boolean>, reject: IFunc<boolean>) => {
                            setTimeout(() => reject(true), TEST_TIME);
                        }).then(() => {
                            assert.fail(null, null, 'Попал в success в отклоненном обещании!');
                        }, (error: boolean) => {
                            return error;
                        }).then((result: boolean) => {
                            assert.equal(result, true, 'Данные переданные в resolve не равны данным из then!');
                            done();
                        });

                    });

                });

            });

            describe('catch', () => {

                it('simple resolve', (done) => {

                    new Promise((resolve: IFunc<{}>) => {
                        setTimeout(() => resolve(true), TEST_TIME);
                    }).catch(() => {
                        assert.fail(null, null, 'Попал в error в resolved обещании!');
                    }).then((data: boolean) => {
                        assert.equal(data, true, 'Данные переданные в resolve не равны данным из then!');
                        done();
                    });

                });

                it('simple reject', (done) => {

                    new Promise((resolve: IFunc<{}>, reject: IFunc<boolean>) => {
                        setTimeout(() => reject(true), TEST_TIME);
                    }).catch((data: boolean) => {
                        assert.equal(data, true, 'Данные переданные в resolve не равны данным из then!');
                        done();
                    });

                });

                it('then after catch', (done) => {

                    new Promise((resolve: IFunc<{}>, reject: IFunc<boolean>) => {
                        setTimeout(() => reject(true), TEST_TIME);
                    }).catch(() => {
                        return 5;
                    }).then((data: number) => {
                        assert.equal(data, 5, 'Данные переданные в resolve не равны данным из then!');
                        done();
                    });

                });

            });

        });

        describe('static', () => {

            let resolve = (data, time?: number) => {
                return new Promise((res: Function) => {
                    setTimeout(() => res(data), Math.floor(time) || TEST_TIME);
                });
            };

            let reject = (data, time?: number) => {
                return new Promise((res: Function, rej: Function) => {
                    setTimeout(() => rej(data), Math.floor(time) || TEST_TIME);
                });
            };

            describe('rase', () => {

                it('resolve', (done) => {
                    Promise.race([resolve(2), resolve(5, TEST_TIME / 2)]).then((data: number) => {
                        assert.equal(data, 5);
                        done();
                    });
                });

                it('reject', (done) => {
                    Promise.race([reject(2), reject(5, TEST_TIME / 2)]).catch((data: number) => {
                        assert.equal(data, 5);
                        done();
                    });
                });

            });

            describe('all', () => {

                it('simple', (done) => {

                    let promises = [resolve(5), resolve(2)];

                    Promise.all(promises).then((data: Array<number>) => {
                        assert.equal(data[0], 5, 'Данные переданные в resolve не равны данным из all!');
                        assert.equal(data[1], 2, 'Данные переданные в resolve не равны данным из all!');
                        done();
                    });

                });

                it('with reject', (done) => {

                    let promises = [reject(5), resolve(2)];

                    Promise.all(promises).then(() => {
                            assert.fail(null, null, 'Попал в success в отклоненном обещании!');
                        },
                        (data: number) => {
                            assert.equal(data, 5, 'Данные переданные в resolve не равны данным из all!');
                            done();
                        });

                });

            });

        });

    });

});
