export class Promise<S, E> {

    private successHandelrs: Array<any> = [];
    private errorHanlers: Array<any> = [];
    private data: S|E;
    private state: boolean;

    constructor(cb: ICallback<S, E>) {

        let resolve: any = (data: S) => {
            this.__setState(data, true);
        };

        let reject = (data: E) => {
            this.__setState(data, false);
        };

        try {
            cb(resolve, reject);
        } catch (e) {
            console.error(e);
            reject(e);
        }
    }

    public then<S, E>(success: IFunc<S>, error?: IFunc<E>): Promise<S, E> {
        return new Promise((resolve: IFunc<S>, reject: IFunc<E>) => {

            let check = (callback: IFunc<S|E>, data: S|E) => {
                try {
                    let result = callback(data);
                    if (result instanceof Promise) {
                        result.then(resolve, reject);
                    } else {
                        resolve(result);
                    }
                } catch (e) {
                    console.error(e);
                    reject(<any>e);
                }
            };

            this.successHandelrs.push((data: S) => {
                check(success, data);
            });
            if (error) {
                this.errorHanlers.push((data: E) => {
                    check(error, data);
                });
            }
            this.__setState(this.data, this.state);
        });
    }

    public catch<S, E>(error: IFunc<E>): Promise<S, E> {
        return this.then((data?: S) => data, error);
    }

    private __setState(data: S|E, state: boolean): void {
        if (state != null) {
            this.data = data;
            this.state = state;
            let callbacks;
            if (state) {
                callbacks = this.successHandelrs.slice();
            } else {
                callbacks = this.errorHanlers.slice();
            }
            this.errorHanlers = [];
            this.successHandelrs = [];
            callbacks.forEach((cb: IFunc<S|E>) => {
                cb(data);
            });
        }
    }

    public static resolve<T, R>(data: T|IPromiseLike<T, any>|Promise<T, R>): Promise<T, R> {
        if (data instanceof Promise) {
            return data;
        }
        if ((<IPromiseLike<T, any>>data).then && typeof (<IPromiseLike<T, any>>data).then === 'function') {
            return new Promise((resolve: IFunc<T>, reject: IFunc<R>) => {
                (<IPromiseLike<T, R>>data).then(resolve, reject);
            });
        } else {
            return (<Promise<T, any>>(new Promise((resolve: IFunc<T>) => {
                resolve(<T>data);
            })));
        }
    }

    public static reject<T>(data: T): Promise<any, T> {
        return new Promise((resolve: IFunc<any>, reject: IFunc<T>) => {
            reject(data);
        });
    }

    public static race<T, R>(promises: Array<Promise<T, R>>): Promise<T, R> {
        return new Promise((resolve: IFunc<T>, reject: IFunc<R>) => {

            let result: Promise<T, R>;
            promises.forEach((promise: Promise<T, R>) => {
                let handler = () => {
                    if (!result) {
                        result = promise;
                        promise.then((data: T) => resolve(data), (data: R) => reject(data));
                    }
                };
                promise.then(handler, handler);
            });

        });
    }

    public static all<S, E>(promises: Array<Promise<S, E>>): Promise<Array<S>, E> {
        return new Promise((resolve: IFunc<Array<S>>, reject?: IFunc<E>) => {

            if (!promises.length) {
                resolve([]);
            }

            let allData = [];
            let resolveCount = 0;

            promises.forEach((promise: Promise<S, E>, index: number) => {

                promise.then((data: S) => {
                    allData[index] = data;
                    resolveCount++;
                    if (resolveCount === promises.length) {
                        resolve(allData);
                    }
                }, (data: E) => {
                    allData[index] = data;
                    reject(data);
                });

            });
        });
    }

}

export interface IPromiseLike<T, R> {
    then: (resolve: IFunc<T>, reject?: IFunc<R>) => any;
}

export interface IFunc<T> {
    (data?: T): any;
}

export interface ICallback<S, E> {
    (resolve: IFunc<S>, reject?: IFunc<E>): any;
}
